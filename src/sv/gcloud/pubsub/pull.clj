(ns sv.gcloud.pubsub.pull
  "Helper functions to pull messages from Google Cloud's PubSub service"
  (:require [clojure.core.async :as a]
            [sv.gcloud.pubsub.client :as p]
            [sv.gcloud.pubsub.retry :as r]))

(defn pull [gcloud-client pull-spec ch-sink]
  (a/go-loop []
    (let [messages (:receivedMessages
                    (:body
                     (r/with-retries
                       "pull pubsub messages"
                       #(gcloud-client
                         (p/pull pull-spec)))))]
      (when (loop [msgs messages]
              (if-let [msg (first msgs)]
                (if (a/>! ch-sink msg)
                  (recur (rest msgs))
                  false)
                true))
        (recur)))))

(defn start [gcloud-client pull-spec]
  (let [ch-sink (a/chan)]
    (pull gcloud-client pull-spec ch-sink)
    ch-sink))

(defn stop [ch-sink]
  (a/close! ch-sink))
