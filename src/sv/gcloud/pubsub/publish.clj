(ns sv.gcloud.pubsub.publish
  (:require [clojure.core.async :as a]
            [sv.gcloud.pubsub.client :as p]
            [sv.gcloud.pubsub.retry :as r]))

(defn publish [gcloud-client publish-spec ch]
  (a/go-loop []
    (when-let [message (a/<! ch)]
      ;; TODO: batch message publish
      (r/with-retries
        "publish pubsub messages"
        #(gcloud-client
          (p/publish
           (assoc publish-spec :messages [{:data message}]))))
      (recur))))

(defn start [ch gcloud-client publish-spec]
  (publish gcloud-client publish-spec ch)
  ch)

(defn stop [ch]
  (a/close! ch))
