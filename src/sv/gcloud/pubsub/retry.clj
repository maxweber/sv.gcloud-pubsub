(ns sv.gcloud.pubsub.retry
  (:require [again.core :as again]
            [clojure.tools.logging :as log]))

(def ^:private exponential-backoff-strategy
  (again/clamp-delay
   5000
   (again/multiplicative-strategy 500 1.5)))

(defn- log-exception [msg f]
  (try
    (f)
    (catch Exception e
      (log/error e msg)
      (throw e))))

(defn with-retries [msg f]
  (again/with-retries
    exponential-backoff-strategy
    (log-exception
     msg
     f)))
