(ns sv.gcloud.pubsub.ack
  (:require [clojure.core.async :as a]
            [sv.gcloud.pubsub.client :as p]
            [sv.gcloud.pubsub.retry :as r]
            [clojure.tools.logging :as log]))

(defn ack-messages [gcloud-client ack-spec ack-ids]
  ;; TODO: handle invalid ackIds
  (log/info "ack ids:" ack-ids)
  (r/with-retries
    "ack pubsub messages"
    #(gcloud-client
      (p/ack
       (-> ack-spec
           (select-keys             
            [:project :subscription])
           (assoc :ackIds ack-ids))))))

(defn ack-loop [gcloud-client ack-spec ch]
  (a/go-loop []
    ;; TODO: batch ack ids with a timeout and a maximum of ack ids
    (when-let [ack-id (a/<! ch)]
      (if (string? ack-id)
        (ack-messages gcloud-client ack-spec [ack-id])
        (log/error "ack-id must be a string"))
      (recur))))

(defn start [gcloud-client ack-spec]
  (let [ch (a/chan)]
    (ack-loop gcloud-client ack-spec ch)
    ch))

(defn stop [ch]
  (a/close! ch))
